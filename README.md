# Repository Contents

This repository contains three different branches, each dedicated to a specific aspect of the project.

## 1. feature-docker
This branch contains a solution for Docker, including a Dockerfile and screenshots.

## 2. feature-iac-pipeline
This branch contains a solution for an Infrastructure as Code (IaC) pipeline configured in GitLab.

## 3. feature-terraform
This branch contains a solution for Terraform code for an AWS architecture.



Author: Netra Kalayshetty